Important: You MUST NOT use ci cache for result & public artifacts !

If build artifacts paths (by default `result/*`) or pages artifacts paths (by default `deploy/*`)
are cached, the ci-runner may introduce packages produced in a previous build in the current one,
thus corrupting the PPA content. You still can use the cache for other paths.

In other words, the .gitlab-ci.yml file MUST NOT contains such a cache definition:

```
#cache:
#  key: "$CI_COMMIT_REF_NAME"
#  paths:
#    - result/*         # don't do this
#    - public/*         # don't do this
```

For details, see [Gitlab issue 336](https://gitlab.com/gitlab-org/gitlab-ci-multi-runner/issues/336)

