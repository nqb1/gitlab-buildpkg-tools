#! /bin/bash
#
#  Copyright (c) 2019 Orange
#
#  Authors:
#    Christian Bayle <christian.bayle@orange.com>
#
#  This program is free software; you can redistribute it and/or modify it
#  under the terms of the GNU General Public License version 2 as published
#  by the Free Software Foundation.
#

. /etc/default/gitlab-buildpkg-repo

DEBIAN_DIST_LIST_DEFAULT="jessie stretch buster bullseye sid experimental"
UBUNTU_DIST_LIST_DEFAULT="trusty xenial yakkety zesty artful bionic cosmic disco eoan focal"
DEBIANALLOW_DEFAULT="oldoldstable>jessie oldstable>stretch stable>buster testing>bullseye unstable>sid $DEBIAN_DIST_LIST_DEFAULT"
UBUNTUALLOW_DEFAULT="$UBUNTU_DIST_LIST_DEFAULT"

DEBIAN_DIST_LIST="${DEBIAN_DIST_LIST:-$DEBIAN_DIST_LIST_DEFAULT}"
UBUNTU_DIST_LIST="${UBUNTU_DIST_LIST:-$UBUNTU_DIST_LIST_DEFAULT}"
DEBIANALLOW="${DEBIANALLOW:-$DEBIANALLOW_DEFAULT}"
UBUNTUALLOW="${UBUNTUALLOW:-$UBUNTUALLOW_DEFAULT}"

ORIGIN="${ORIGIN:-$(hostname -f)}"
STAGING_SIGNWITH="${STAGING_SIGNWITH:-staging@$ORIGIN}"
PRODUCTION_SIGNWITH="${PRODUCTION_SIGNWITH:-production@$ORIGIN}"
VERSION="${VERSION:-5.0}"

DEBIAN_COMPONENTS="${DEBIAN_COMPONENTS:-main}"
UBUNTU_COMPONENTS="${UBUNTU_COMPONENTS:-main}"

gen_debian_distribution(){
	for dir in debian debian/UploadQueue html html/debian html/debian/conf 
	do 
		[ -d $dir ] || mkdir $dir
	done
	if [ -f html/debian/conf/distributions ] ; then
	    mv html/debian/conf/distributions html/debian/conf/distributions.old
	fi
	mkdir -p html/debian/conf/distributions
	for DIST in $DEBIAN_DIST_LIST ; do
	    [ ! -f html/debian/conf/distributions/${DIST}.conf ] || continue
	    cat <<-EOF
Codename: $DIST
Suite: $DIST
Components: $DEBIAN_COMPONENTS
UDebComponents: $DEBIAN_COMPONENTS
Architectures: armhf amd64 i386 source
Origin: $ORIGIN
Version: $VERSION
Tracking: all includechanges
Description: Debian $DIST repository for Gitlab CI
SignWith: $SIGNWITH

EOF
	done > html/debian/conf/distributions/${DIST}.conf
}

gen_debian_incoming(){
	if [ -f html/debian/conf/incoming ] ; then
	    mv html/debian/conf/incoming html/debian/conf/incoming.old
	fi
	mkdir -p html/debian/conf/incoming
	cat > html/debian/conf/incoming/default.conf <<-EOF
Name: default
IncomingDir: /var/lib/gitlab-buildpkg-repo/$(id -un)/debian/UploadQueue
TempDir: tmp
Cleanup: on_error
Allow: $DEBIANALLOW
EOF
}

gen_ubuntu_distribution(){
	for dir in ubuntu ubuntu/UploadQueue html html/ubuntu html/ubuntu/conf 
	do 
		[ -d $dir ] || mkdir $dir
	done
	if [ -f html/ubuntu/conf/distributions ] ; then
	    mv html/ubuntu/conf/distributions html/ubuntu/conf/distributions.old
	fi
	mkdir -p html/ubuntu/conf/distributions

	for DIST in $UBUNTU_DIST_LIST ; do
	    [ ! -f html/ubuntu/conf/distributions/${DIST}.conf ] || continue
	    cat <<-EOF
Codename: $DIST
Suite: $DIST
Components: $UBUNTU_COMPONENTS
UDebComponents: $UBUNTU_COMPONENTS
Architectures: armhf amd64 i386 source
Origin: $ORIGIN
Version: $VERSION
Tracking: all includechanges
Description: Ubuntu $DIST repository for Gitlab CI
SignWith: $SIGNWITH

EOF
	done > html/ubuntu/conf/distributions/${DIST}.conf
}

gen_ubuntu_incoming(){
	if [ -f html/ubuntu/conf/incoming ] ; then
	    mv html/ubuntu/conf/incoming html/ubuntu/conf/incoming.old
	fi
	mkdir -p html/ubuntu/conf/incoming
	cat > html/ubuntu/conf/incoming/default.conf <<-EOF
Name: default
IncomingDir: /var/lib/gitlab-buildpkg-repo/$(id -un)/ubuntu/UploadQueue
TempDir: tmp
Cleanup: on_error
Allow: $UBUNTUALLOW
Permit: unused_files
EOF
}

gen_gnupg_key(){
	export GNUPG_MAIL="$1"
	if ! gpg --list-secret-keys $GNUPG_MAIL >/dev/null
	then
		echo "Generate a GNUPG bot key pair"
		cat > ~/.gnupgkeyfile <<-EOF
%no-protection
%echo Generating a standard key
Key-Type: RSA
Key-Length: 4096
Subkey-Type: ELG-E
Subkey-Length: 4096
Name-Real: Bot key
Name-Comment: with stupid passphrase
Name-Email: $GNUPG_MAIL
Expire-Date: 0
#Passphrase: abc
#%pubring botkey.pub
#%secring botkey.sec
# Do a commit here, so that we can later print "done" :-)
%commit
%echo done
EOF
		gpg --batch --gen-key ~/.gnupgkeyfile
		# Trust Ultimate (required not to have error)
		echo -e "5\ny\n" |  gpg --command-fd 0 --expert --edit-key $GNUPG_MAIL trust
	else 
		echo "Use existing GNUPG bot key pair"
		gpg --list-secret-keys
	fi
}

case $(id -un) in
	staging)
		SIGNWITH="$STAGING_SIGNWITH"
		export SIGNWITH
		;;
	production)
		SIGNWITH="$PRODUCTION_SIGNWITH"
		export SIGNWITH
		;;
esac

gen_gnupg_key $SIGNWITH
gen_debian_distribution 
gen_debian_incoming
gen_ubuntu_distribution
gen_ubuntu_incoming

reprepro -Vb html/debian export
reprepro -Vb html/debian check
reprepro -Vb html/ubuntu export
reprepro -Vb html/ubuntu check
gpg --export --armor $SIGNWITH > html/key 

#DEBHELPER#

exit 0
