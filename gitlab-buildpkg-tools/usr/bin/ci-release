#!/bin/bash
# 
#  Copyright (c) 2017 Orange
#  
#  Authors:
#    Christian Bayle <christian.bayle@orange.com>
#    Michel Decima <michel.decima@orange.com>
#  
#  This program is free software; you can redistribute it and/or modify it
#  under the terms of the GNU General Public License version 2 as published 
#  by the Free Software Foundation.
# 

# IMPORTANT: see 'curl -sSL https://get.docker.com/' for a better algo
# 
# Release identification : set variables RELEASE_DIST and RELEASE_NAME 
# from current system info, in order to sort packages in a repository.
#
# RELEASE_DIST is a lower-case synonym for the distributor id, like debian,
# ubuntu, centos, fedora, rhel...
# RELEASE_NAME is short codename version in lower case for debian-like,
# and decimal part of release version id for .rpm based distribs.
#
# Examples:
#   Debian GNU/Linux 8.7 (jessie)                       -> debian/jessie
#   Ubuntu 16.04.2 LTS                                  -> ubuntu/xenial
#   CentOS Linux release 7.3.1611 (Core)                -> centos/7
#   Fedora release 25 (Twenty Five)                     -> fedora/25
#   openSUSE 42.2 (Malachite)                           -> opensuse/42
#   Red Hat Enterprise Linux Server release 7.1 (Maipo) -> rhel/7
# 

if [ -f /etc/system-release ] ;  then

    case $(cat /etc/system-release) in
        CentOS*)        RELEASE_DIST="centos" ;;
        Fedora*)        RELEASE_DIST="fedora" ;;
        Red*)           RELEASE_DIST="rhel" ;;
        *)              RELEASE_DIST="unknown" ;;
    esac
    RELEASE_NAME=$(grep -E -o '[0-9.]{1,}' /etc/system-release | cut -d. -f1)
    RELEASE_NUM=$(printf %04d "$RELEASE_NAME")

elif [ -f /etc/SuSE-release ] ; then

    RELEASE_DIST=$(awk -F= '/^ID=/ {print $2}' /etc/os-release)
    RELEASE_NAME=$(awk -F= '/^VERSION_ID=/ {print $2}' /etc/os-release | tr -d '"' | cut -d. -f1 )

elif [ -f /etc/os-release ] ; then
    . /etc/os-release
    RELEASE_DIST="$ID"
    if [ -z "$VERSION_CODENAME" ] ; then
        if [ -x /usr/bin/lsb_release ] ; then
            RELEASE_NAME=$(lsb_release -c | cut -f2 | tr '[:upper:]' '[:lower:]')
        else
            if [ -z "$VERSION" ] ; then
                if [ -z "$PRETTY_NAME" ] ; then
                    RELEASE_NAME=unknown
                else
                    RELEASE_NAME=$(echo "$PRETTY_NAME" | cut -d' ' -f3 | cut -d/ -f1)
                fi
            else
                # shellcheck disable=SC2001
                RELEASE_NAME=$(echo "$VERSION" | sed 's/.*(\(.*\)).*/\1/')
            fi
        fi
    else
        RELEASE_NAME="$VERSION_CODENAME"
    fi
    if [ -z "$VERSION_ID" ] ; then
        case "$RELEASE_NAME" in 
           bullseye)
                RELEASE_NUM=0011
                ;;
           bookworm)
                RELEASE_NUM=0012
                ;;
           trixie)
                RELEASE_NUM=0013
                ;;
           *)
                RELEASE_NUM=0000
                ;;
        esac
    else
        # shellcheck disable=SC2001
        VERSION_ID=$(echo "$VERSION_ID" | sed 's/\.//')
        RELEASE_NUM=$(printf %04d "$VERSION_ID")
    fi

elif [ -x /usr/bin/lsb_release ] ; then

    case $(lsb_release -i | cut -f2) in
        LinuxMint)      RELEASE_DIST="mint" ;;
        Debian)         RELEASE_DIST="debian" ;;
        Ubuntu)         RELEASE_DIST="ubuntu" ;;
        *)              RELEASE_DIST="unknown" ;;
    esac
    RELEASE_NAME=$(lsb_release -c | cut -f2 | tr '[:upper:]' '[:lower:]')
    if [ "$RELEASE_NAME" = "n/a" ] ; then
        # testing or sid (?)
        RELEASE_NAME="notavail"
    fi

else

    RELEASE_DIST="unknown"
    RELEASE_NAME="unknown" 
    RELEASE_NUM="unknown" 

fi

case $1 in
    --full)
        echo "$RELEASE_DIST/$RELEASE_NAME/$RELEASE_NUM"
        ;;
    *)
        echo "$RELEASE_DIST/$RELEASE_NAME"
        ;;
esac
