#!/bin/bash
# 
#  Copyright (c) 2017 Orange
#  
#  Authors:
#    Christian Bayle <christian.bayle@orange.com>
#    Michel Decima <michel.decima@orange.com>
#  
#  This program is free software; you can redistribute it and/or modify it
#  under the terms of the GNU General Public License version 2 as published 
#  by the Free Software Foundation.
# 

die() {
    echo "${0##*/}: $*" >&2 ; exit 1
}

log_section() {
   printf '=%.0s' {1..72} ; printf "\n"
   printf "=\t%s\n" "" "$@" ""
}

configure_and_check() {
    PAGES_DIR=${PAGES_DIR:-public}
    CI_PROJECT_NAME=${CI_PROJECT_NAME:-project-name}
   
    declare -p PAGES_DIR CI_PROJECT_NAME
    
    [ -z "$PAGES_DIR" ] && die "not set: PAGES_DIR"
    mkdir -p "$PAGES_DIR" || die "mkdir failed: $PAGES_DIR"
}


#############################################################################

css_simple() {
    cat << EOF_css
table {
    overflow:auto;
    width:95%;
    margin:1.625em auto;
    border-spacing:2px;
    border-collapse:separate;
    vertical-align:middle;
}

table td {
    text-align: right;
}

table td:first-child {
    text-align: left;
}

table thead td {
    padding:0.5em;
    background-color:rgb(102, 207, 255);
}

table th a {
    color:rgb(158, 188, 235);
}

table tbody td {
    padding:0.2em 0.5em;
}

table tbody tr {
    background-color:rgb(255, 255, 255)
}

table tbody tr:nth-child(odd) {
    background-color:rgb(230, 247, 255)
}

table tbody tr:hover{
    background-color:rgb(230, 230, 230)
}
EOF_css
}

dir_list() {
    local dirname=${1:-.}
    printf "..||\n"
    (
        cd "$dirname" && while read -r filename ; do
            stat --printf='%n|%.16y|%s\n' "$filename"
        done < <(ls -1p --group-directories-first)
    )
}

list_to_html() {
    printf '<table>\n'
    printf '<thead>\n'
    printf '<tr><td>Name</td><td>Last modified</td><td>Size</td></tr>\n'
    printf '</thead>\n'
    printf '<tbody>\n'
    local old_IFS=$IFS ; IFS='|'
    while read -r filename lastmodif filesize ; do
        local href="$filename"
        case "$filename" in
            ..) filename="[Parent directory]" ;;
            */) filesize="&nbsp;"
        esac
        printf '<tr><td><a href="%s">%s</a></td><td>%s</td><td>%s</td></tr>\n' \
            "$href" "$filename" "$lastmodif" "$filesize"
    done
    IFS=$old_IFS
    printf '</tbody>\n'
    printf '</table>\n'
}

html() {
    local dirname=${1:-.} pathname=$CI_PROJECT_NAME/${dirname/#.*public\//}
    cat << EOF_html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>$pathname</title>
  <style>
$(css_simple)
  </style>
</head>
<body>
  <h2>Index of $pathname</h2>
  <hr>
$(dir_list "$dirname" | list_to_html)  
</body>
</html>
EOF_html
}

#############################################################################

log_section "Configure and check"
configure_and_check

log_section "Generate indexes"
for dir in $(find "$PAGES_DIR" -type d | sort -u) ; do
    if [ ! -f "$dir"/index.html ] ; then
        echo "$dir"/index.html
        html "$dir" > "$dir"/.index.html
        mv "$dir"/.index.html "$dir"/index.html
    fi
done

log_section "Artifacts..."
tree "$PAGES_DIR"

