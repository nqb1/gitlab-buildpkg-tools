#!/bin/bash
# 
#  Copyright (c) 2017 Orange
#  
#  Authors:
#    Christian Bayle <christian.bayle@orange.com>
#    Michel Decima <michel.decima@orange.com>
#  
#  This program is free software; you can redistribute it and/or modify it
#  under the terms of the GNU General Public License version 2 as published 
#  by the Free Software Foundation.
# 

# usage: ci-pages-home dir

die() {
    echo "${0##*/}: $*" >&2 ; exit 1
}

log_section() {
   printf '=%.0s' {1..72} ; printf "\n"
   printf "=\t%s\n" "" "$@" ""
}

configure_and_check(){
    PAGES_HOST=${PAGES_HOST:-gitlab.io}
    PAGES_DIR=${PAGES_DIR:-public}
    CI_PROJECT_NAMESPACE=${CI_PROJECT_NAMESPACE:-project-namespace}
    CI_PROJECT_NAME=${CI_PROJECT_NAME:-project-name}
        CI_PAGES_URL=${CI_PAGES_URL:-pages-url}

    declare -p PAGES_DIR PAGES_HOST CI_PROJECT_NAMESPACE CI_PROJECT_NAME CI_PAGES_URL

    [ -z "$PAGES_DIR" ] && die "not set: PAGES_DIR"
    [ -z "$CI_PROJECT_NAMESPACE" ] && die "not set: CI_PROJECT_NAMESPACE"
    [ -z "$CI_PROJECT_NAME" ] && die "not set: CI_PROJECT_NAME"
    mkdir -p "$PAGES_DIR" || die "mkdir failed: $PAGES_DIR"

    PPA_NAME="$CI_PROJECT_NAMESPACE/$CI_PROJECT_NAME"
    PPA_SLUG=$(tr '/[:upper:]' '-[:lower:]' <<< "$PPA_NAME")
    # shellcheck disable=SC2001
    PPA_URL=$(sed 's|/*$||' <<< "$CI_PAGES_URL")
    declare -p PPA_NAME PPA_SLUG PPA_URL
    # TODO PAGES_HOST not used anymore, remove variable
}

find_deb_releases() {
    local topdir=${PAGES_DIR:-.}
    find "$topdir" -type f -name "InRelease" \
        | awk -F/ '{print $(NF-3)"/"$(NF-1)}' | sort -u
}

find_rpm_releases() {
    local topdir=${PAGES_DIR:-.}
    find "$topdir" -type d -name "repodata" \
        | awk -F/ '{print $(NF-3)"/"$(NF-2)}' | sort -u
}

span_class() {
    printf '<span class="%s">%s</span>' "$1" "$2"
}

form_options() {
    for option in "$@" ; do
        printf '            <option value=%s>%s</option>\n' "$option" "$option"
    done
}

html_header() {
    cat << EOF_header
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes">
    <title>Gitlab PPA: $PPA_NAME</title>
    <style>
        pre {
            border: 1px dotted gray;
            background-color: #ececec;
            color: #111111;
            padding: 0.5em;
        }
        code {
            font-family: monospace;
            white-space: pre;
        }
    </style>
    <script>
    <!--
        function update_deb_release(select) {
            var release = select.value.split("/")
            var id_elts=document.getElementsByClassName("deb_release_id")
            for (var i=0; i<id_elts.length; ++i) {
                id_elts[i].innerHTML = release[0] ;
            }
            var name_elts=document.getElementsByClassName("deb_release_name")
            for (var i=0; i<name_elts.length; ++i) {
                name_elts[i].innerHTML = release[1] ;
            }
        }

        function update_rpm_release(select) {
            var release = select.value.split("/")
            var id_elts=document.getElementsByClassName("rpm_release_id")
            for (var i=0; i<id_elts.length; ++i) {
                id_elts[i].innerHTML = release[0] ;
            }
            var name_elts=document.getElementsByClassName("rpm_release_name")
            for (var i=0; i<name_elts.length; ++i) {
                name_elts[i].innerHTML = release[1] ;
            }
        }
    //-->
    </script>
</head>
<body>
EOF_header
}


html_footer() {
    cat << EOF_footer
</body>
</html>
EOF_footer
}

html_meta() {
    cat << EOF_meta
    <h1 id="h1-meta">Gitlab PPA: $PPA_NAME</h1>
    <ul>
        <li>Project:   <a href="$CI_PROJECT_URL">$CI_PROJECT_URL</a></li>
        <li>Reference: <a href="$CI_PROJECT_URL/tree/$CI_COMMIT_REF_NAME">$CI_COMMIT_REF_NAME</a></li>
        <li>Commit:    <a href="$CI_PROJECT_URL/commit/$CI_COMMIT_SHA">$CI_COMMIT_SHA</a></li>
        <li>Pipeline:  <a href="$CI_PROJECT_URL/pipelines/$CI_PIPELINE_ID">#$CI_PIPELINE_ID</a></li>
        <li>Date:      $(date --iso-8601=seconds)</li>
    </ul>
EOF_meta
}

list_dir() {
    ls -1 -p --group-directories-first "$@"
}


html_content() {
    cat << EOF_content
    <h2 id="h2-content">Content</h2>
    <ul>
    $(list_dir "$PAGES_DIR" | grep -v "index.html$" \
        | awk '{printf "        <li><a href=\"%s\">%s</a></li>\n", $1, $1}')
    </ul>
EOF_content
}


html_config_deb_auto() {
    cat << EOF_config_deb_auto
    <h2 id="deb-config">Configuration for Debian-like distribution</h2>
    <h3 id="deb-config-auto">Automatic sources.list configuration</h3>
    <pre><code>sudo apt-get install apt-add-gitlab
sudo apt-add-gitlab $PPA_URL
</code></pre>
EOF_config_deb_auto
}

html_config_deb_manual() {
    local releases=( "$@" )
    local src_list="/etc/apt/sources.list.d/gitlab-$PPA_SLUG.list"
    local key_file="/etc/apt/trusted.gpg.d/gitlab-$PPA_SLUG.asc"

    cat << EOF_config_deb_manual
    <h3 id="deb-config-manual">Manual sources.list configuration</h3>
    <h4 id="deb-config-manual-common">For all distributions</h4>
    <pre><code>sudo curl -L $PPA_URL/GPG_PUBLIC_KEY -o $key_file
sudo apt-get install apt-transport-https
</code></pre>
    <h4 id="deb-config-manual-release">For selected release</h4>
    <form>
        <select id="deb.series" name="deb.series" size="1" onChange='update_deb_release(this);'>
            <option selected="selected" value="RELEASE_ID/RELEASE_NAME">Choose your release version</option>
$(form_options "${releases[@]}")
        </select>
    </form>
    <pre><code>sudo tee $src_list << EOF
deb [arch=amd64] $PPA_URL/$(span_class deb_release_id RELEASE_ID) $(span_class deb_release_name RELEASE_NAME) main
#deb-src $PPA_URL/$(span_class deb_release_id RELEASE_ID) $(span_class deb_release_name RELEASE_NAME) main
EOF
</code></pre>
EOF_config_deb_manual
}


html_config_rpm_auto() {
    cat << EOF_config_rpm_auto
    <h2 id="rpm-config">Configuration for RPM-based distribution</h2>
    <h3 id="rpm-config-auto">Automatic yum.conf configuration</h3>
    <pre><code>sudo yum install yum-add-gitlab
sudo yum-add-gitlab $PPA_URL
</code></pre>
EOF_config_rpm_auto
}

html_config_rpm_manual() {
    local releases=( "$@" )
    local yum_conf="/etc/yum.repos.d/gitlab-$PPA_SLUG.repo"

    cat << EOF_config_rpm_manual
    <h3 id="rpm-config-manual">Manual yum.conf configuration</h3>
    <h4 id="rpm-config-manual-common">For all distributions</h4>
    <pre><code>sudo rpm --import $PPA_URL/GPG_PUBLIC_KEY
</code></pre>
    <h4 id="rpm-config-manual-release">For selected release</h4>
    <form>
        <select id="rpm.series" name="rpm.series" size="1" onChange='update_rpm_release(this);'>
            <option selected="selected" value="RELEASE_ID/RELEASE_NAME">Choose your release version</option>
$(form_options "${releases[@]}")
         </select>
    </form>
    <pre><code>sudo tee $yum_conf << EOF
[$PPA_SLUG]
name=Gitlab $PPA_SLUG - $(span_class rpm_release_name RELEASE_NAME) - \\\$basearch
baseurl=$PPA_URL/$(span_class rpm_release_id RELEASE_ID)/$(span_class rpm_release_name RELEASE_NAME)/\\\$basearch
enabled=1
metadata_expire=15m
gpgcheck=1
repo_gpgcheck=0
gpgkey=$PPA_URL/GPG_PUBLIC_KEY
skip_if_unavailable=False
EOF
</code></pre>
EOF_config_rpm_manual
}

#[${PPA_SLUG}-source]
#name=Gitlab $PPA_SLUG - Source 
#baseurl=$PPA_URL/$(span_class rpm_release_id RELEASE_ID)/$(span_class rpm_release_name RELEASE_NAME)/Source
#enabled=0
#metadata_expire=15m
#gpgcheck=1
#gpgkey=$PPA_URL/GPG_PUBLIC_KEY
#skip_if_unavailable=False


html() {
    html_header
    html_meta
    html_content

    readarray -t deb_releases < <(find_deb_releases "$PAGES_DIR")
    if [ ${#deb_releases[@]} -gt 0 ] ; then
        html_config_deb_auto
        html_config_deb_manual "${deb_releases[@]}"
    fi

    readarray -t rpm_releases < <(find_rpm_releases "$PAGES_DIR")
    if [ ${#rpm_releases[@]} -gt 0 ] ; then
        html_config_rpm_auto
        html_config_rpm_manual "${rpm_releases[@]}"
    fi

    html_footer
}

###########################################################

log_section "Configure and check"
configure_and_check

log_section "Generate Readme files"
#html | tee $PAGES_DIR/.index.html
html > "$PAGES_DIR/.index.html"
mv -v "$PAGES_DIR/.index.html" "$PAGES_DIR/index.html"
tree --prune -P "README*|index.html" "$PAGES_DIR"

